# Tarea 2 - Arquitecutra de software
Esteban Esparza - Marzo 2023

## Descripción
Se implementó un software de analisis de proyecto en java, capaz de calcular metricas de arquitectura de software. En este caso el software calcula: 
* El grado de abstración del proyecto.
* El acomplamiento Eferente y Aferente
* La inestabilidad promedio del proyecto.
* La distancia con la secuencia del punto (ins,abs)

Es software funciona mediante la terminal utilizando node y genera la representacion grafica de la distancia de la secuencia principal en una imagen .png alojada en la raiz del proyecto.

## Plugins disponibles
- abstraction
- instability
- chart
## Guia de uso
El comando de ejecución es el siguiente
```
npm run dev RUTA_DEL_PROYECTO plugin_1 plugin_n
```
### Consideraciones
* Para la ejecución del software y un correcto analisis se debe tener un proyecto ya descargado dentro del sistema.
* El software unicamente analiza un proyecto a la vez.
* Unicamente analiza proyectos en Java

## Librerias usadas
* chartjs-to-image
* typescript
* jest
* jsdoc
* npmlog
* npmlog-file

## Modulo Scanner

* Este modulo se encarga de recorrer un directorio de proyecto, leer todos los archivos Java en él,
procesar la información relevante y generar una lista de `JavaFile` para posteriormente construir un objeto `Scanner` con todos los datos requeridos por el resto de modulos.

## Modulo Metrics
### Abstraction
   * Calcula el grado de abstracción de un conjunto de archivos en un proyecto de software.
   * El grado de abstracción mide qué porcentaje de los archivos son abstractos.

### Instability
   * Calcula la inestabilidad de un proyecto de software.
   * La inestabilidad es una medida de la proporción de dependencias eferentes y aferentes de un módulo.

## Modulo Chart

 * Genera una gráfica que representa la distancia de una secuencia de clases a la secuencia principal de un proyecto de software.




## Diagrama de Clases
![](https://i.imgur.com/USKoOnh.png)