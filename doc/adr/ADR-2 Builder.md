# ADR-2. Implementación del patrón de diseño "Builder" en la clase Scanner.
# Situación

Aceptado.

# Contexto

La clase Scanner se encarga de escanear un proyecto y recopilar información sobre los archivos Java encontrados. La lógica de escaneo y construcción de la lista de archivos está contenida en el método scanProject(). Sin embargo, la implementación actual de la clase Scanner resulta compleja y poco legible debido a la acumulación de lógica en un solo método.

# Decisión

Se decide implementar el patrón de diseño "Builder" en la clase Scanner para mejorar su legibilidad y facilitar su mantenimiento. El patrón de diseño "Builder" se encargará de encapsular la lógica de construcción de la lista de archivos y proporcionar una interfaz más clara y estructurada para configurar y construir el objeto Scanner.

Se creará una clase ScannerBuilder que actuará como un constructor configurable para la clase Scanner. La clase ScannerBuilder permitirá establecer el proyecto a escanear y proporcionará métodos claros y separados para configurar diferentes aspectos del escaneo, como la exclusión de archivos o la validación de tipos. Una vez configurado, se invocará el método build() de ScannerBuilder para obtener una instancia de la clase Scanner completamente configurada y lista para su uso.

# Consecuencias

- Mejora de la legibilidad y mantenibilidad del código al separar la lógica de construcción de la clase Scanner en una clase Builder dedicada.

- Permite agregar nuevos métodos a ScannerBuilder para extender la funcionalidad de configuración sin afectar directamente la clase Scanner.
- Promueve una estructura más modular al separar la construcción del objeto Scanner de la lógica de escaneo en sí misma.
- Puede aumentar la complejidad del código al introducir una clase adicional (ScannerBuilder)

# Cumplimiento

El cumplimiento de esta decisión se puede verificar mediante las siguientes acciones:

- Realizar pruebas unitarias exhaustivas para garantizar que el proceso de construcción de Scanner a través de ScannerBuilder funcione correctamente.
- Verificar que los métodos de configuración en ScannerBuilder se comporten según lo esperado y permitan establecer diferentes opciones de escaneo, como la exclusión de archivos o la validación de tipos.
- Realizar pruebas de integración para asegurarse de que la instancia de Scanner construida por ScannerBuilder esté correctamente configurada y pueda escanear el proyecto de manera precisa.
# Notas

Autor: Esteban Esparza e.esparza02@ufromail.cl
Día de publicación: 23-05-2023
Última actualización: 25-05-2023