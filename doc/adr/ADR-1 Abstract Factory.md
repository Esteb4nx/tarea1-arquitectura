# ADR-1. Implementación del patrón de diseño Abstract Factory
## Situación

Aceptado

# Contexto

El proyecto en cuestión se basa en una arquitectura de microkernel, lo que requiere un alto grado de desacoplamiento entre los distintos componentes o plugins del sistema. Para ello, es necesario contar con una forma flexible y escalable de crear instancias de estos plugins. Además, se anticipa que el número y la variedad de estos plugins puedan aumentar en el futuro, lo que requiere un enfoque que pueda manejar esta creciente complejidad.

## Decisión
Para manejar esta situación, se ha decidido implementar el patrón de diseño Abstract Factory. Este patrón permite crear familias de objetos relacionados sin especificar sus clases concretas, facilitando la creación de diferentes plugins sin acoplar el código a clases específicas. Esto facilitará la escalabilidad del proyecto, ya que se podrán añadir nuevos tipos de plugins simplemente extendiendo las factorías existentes o añadiendo nuevas. Además, este patrón también facilitará el intercambio de plugins en tiempo de ejecución, proporcionando una gran flexibilidad al sistema.

## Consecuencias
- La implementación del patrón Abstract Factory implicará una mayor complejidad inicial en la arquitectura del software, ya que se requiere una planificación y diseño cuidadoso para establecer las abstracciones correctas. Sin embargo, a largo plazo, esto resultará en un sistema más modular y mantenible. Por otro lado, el sistema será más escalable y flexible, ya que facilita la incorporación de nuevos tipos de plugins. Finalmente, este patrón podría llevar a una sobreabstracción si se usa incorrectamente, por lo que es importante utilizarlo con moderación y solo cuando sea realmente beneficioso.

## Cumplimiento
- El cumplimiento de esta decisión se puede medir a través de la capacidad del sistema para incorporar nuevos tipos de plugins sin realizar grandes modificaciones en el código existente. Además, la capacidad del sistema para intercambiar plugins en tiempo de ejecución sin errores o problemas también será un indicador del éxito de esta implementación.

# Notas

Autor: Esteban Esparza e.esparza02@ufromail.cl
Día de publicación: 23-05-2023
Última actualización: 25-05-2023