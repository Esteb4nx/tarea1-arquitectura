import { Abstraction } from "../src/domain/metrics/components/abstraction";
import { Type } from "../src//models/types";
import { JavaFile } from "../src/models/javaFile";

describe("Abstraction", () => {
    let abstraction: Abstraction;
    let files: JavaFile[];

    beforeEach(() => {
        abstraction = new Abstraction();
        files = [
            { fileName: 'file1', filePath: 'path1', fileData: 'data1', type: Type.ABSTRACT },
            { fileName: 'file2', filePath: 'path2', fileData: 'data2', type: Type.ABSTRACT },
            { fileName: 'file3', filePath: 'path3', fileData: 'data3', type: Type.CONCRETE },
            { fileName: 'file4', filePath: 'path4', fileData: 'data4', type: Type.CONCRETE },
            { fileName: 'file5', filePath: 'path5', fileData: 'data5', type: Type.CONCRETE },
        ];
    });

    it("should correctly calculate the degree of abstraction", async () => {
        const expectedDegreeOfAbstraction = 2 / 5;  // 2 abstract files, 5 total files
        const result = await abstraction.execute(files);
        expect(result).toEqual(expectedDegreeOfAbstraction);
    });

    it("should return 0 when there are no abstract files", async () => {
        files.forEach(file => file.type = Type.CONCRETE);  // Make all files concrete
        const result = await abstraction.execute(files);
        expect(result).toEqual(0);
    });

    it("should return 1 when all files are abstract", async () => {
        files.forEach(file => file.type = Type.ABSTRACT);  // Make all files abstract
        const result = await abstraction.execute(files);
        expect(result).toEqual(1);
    });
});
