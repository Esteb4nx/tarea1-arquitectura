import { Instability } from '../src/domain/metrics/components/instability';
import { JavaFile } from '../src/models/javaFile';
import { Type } from '../src/models/types';

const fs = require('fs');
jest.mock('fs');

describe('Instability', () => {
    const javaFile1: JavaFile = {
        fileName: 'File1.java',
        filePath: '/path/to/File1.java',
        fileData: 'Some data...',
        type: Type.CONCRETE
    };

    const javaFile2: JavaFile = {
        fileName: 'File2.java',
        filePath: '/path/to/File2.java',
        fileData: 'Some other data...',
        type: Type.CONCRETE
    };

    const files = [javaFile1, javaFile2];

    let instability: Instability;
    beforeEach(() => {
        instability = new Instability();
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should calculate instability', async () => {
        (fs.readFileSync as jest.MockedFunction<typeof fs.readFileSync>).mockReturnValue(javaFile1.fileData);
        const result = await instability.execute(files);
        expect(typeof result).toBe('number');
    });
    


    it('should calculate efferent coupling', () => {
        const data = 'import File1;\nimport File2;';
        const result = instability.getImports(data, files);
        expect(result).toEqual(2);
    });

    it('should calculate afferent coupling', () => {
        (fs.readFileSync as jest.MockedFunction<typeof fs.readFileSync>).mockReturnValueOnce(javaFile1.fileData).mockReturnValueOnce(javaFile2.fileData);
        const result = instability.getCalls(files, javaFile1);
        expect(typeof result).toBe('number');
    });
});
