export interface IPlugin {
    execute(params: any): Promise<any>;
}