const ChartJsImage = require("chartjs-to-image");
import {Point} from "../models/point";

export class Distance{
    
    public intersectionPoint(abs: number,ins: number) {   
        let perpendicularSlope: number = -1 / -1;
        let perpendicularIntersection: number = abs - perpendicularSlope * ins;
    
        let xIntersection: number = (1 - perpendicularIntersection) / (perpendicularSlope - -1);
        let yIntersection: number = this.equationLine(xIntersection);
    
        let intersectionPoint: Point = {
          x: xIntersection,
          y: yIntersection
        };

        return intersectionPoint;
    }

    public distance(abs: number,ins: number) {
        return Math.abs( abs + ins - 1 );
    }

    private equationLine(x: number): number {
        return -1 * x + 1;
      };
}