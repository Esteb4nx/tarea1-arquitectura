const ChartJsImage = require("chartjs-to-image");
import { IPlugin } from "../../../interfaces/IPlugin";
import { JavaFile } from "../../../models/javaFile";
import { Distance } from "../utils/distance";
const log = require("npmlog");

export class Chart implements IPlugin {
  private distance: Distance = new Distance();

  async execute(params: {
    abs: number;
    ins: number;
    imageName: string;
  }): Promise<any> {
    let distance = this.generateChart(params.abs, params.ins, params.imageName);
    return distance;
  }

  /**
   * Genera una gráfica que representa la distancia de una secuencia de clases a la secuencia principal de un proyecto de software.
   *
   * @param {number} abs - El valor de la distancia en el eje y desde la secuencia principal hasta la secuencia de clases abstractas.
   * @param {number} ins - El valor de la distancia en el eje x desde la secuencia principal hasta la primera secuencia de clases concretas.
   * @returns {distance} Esta función retorna la distancia de la secuencia principal y guarda una imagen de la gráfica generada en el archivo "DistanciaDeLaSecuenciaPrincipal.png".
   */

  public generateChart(abs: number, ins: number, IMAGE_NAME: string) {
    const chart = new ChartJsImage();

    const intersectionPoint = this.distance.intersectionPoint(abs, ins);

    const distance = this.distance.distance(abs, ins);
    log.info("Distancia de la secuencia principal: " + distance);

    chart.setConfig({
      type: "scatter",
      data: {
        datasets: [
          {
            type: "line",
            label: "Secuencia principal",
            fill: false,
            data: [
              { x: 0, y: 1 },
              { x: 1, y: 0 },
            ],
          },
          {
            type: "line",
            label: "P",
            borderColor: "red",
            fill: false,
            borderDash: [3, 3],
            data: [
              { x: ins, y: abs },
              { x: intersectionPoint.x, y: intersectionPoint.y },
            ],
          },
        ],
      },
    });
    chart.toFile(IMAGE_NAME);
    log.info("Grafico generado y guardado como: " + IMAGE_NAME);
    return distance;
  }

  private equationLine(x: number): number {
    return -1 * x + 1;
  }
}
