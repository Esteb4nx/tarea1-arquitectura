import { Type } from '../../../models/types';

const fs = require("fs");

/**
 * Clase `Validator` que proporciona funciones para validar ciertos aspectos de los archivos Java en un proyecto.
 *
 * Esta clase incluye métodos para determinar el tipo de un archivo Java y para verificar si un archivo es una prueba.
 */
export class Validator{

  constructor(){
  }

  /**
   * Obtiene el tipo de un archivo Java.
   *
   * Esta función determina si un archivo Java es abstracto, concreto u otro basándose en su contenido.
   *
   * @param filePath - La ruta al archivo Java.
   * @param fileName - El nombre del archivo Java.
   * @returns El tipo de archivo Java.
   */
    public getType(filePath: string, fileName: string): Type{
        if (fileName == "package-info.java") {
            return Type.OTHER;
          }
        
          let data = fs.readFileSync(filePath.replace(/\\/g, "/"), "utf-8");
        
          let lineas = data.split("\n");
          const lineaDeclaracionClase = lineas.find((linea: string | string[]) =>
            linea.includes(fileName.split(".")[0])
          );
        
          if (
            lineaDeclaracionClase.includes("interface") ||
            lineaDeclaracionClase.includes("abstract")
          ) {
            return Type.ABSTRACT;
          } else {
            return Type.CONCRETE;
          }
    }

    /**
     * Verifica si un archivo Java es una prueba.
     *
     * Esta función verifica si un archivo Java es una prueba buscando las anotaciones @Test y @ParameterizedTest en su contenido.
     *
     * @param filePath - La ruta al archivo Java.
     * @returns Un valor booleano que indica si el archivo es una prueba.
     */
    public isTest(filePath: string): boolean{
        let data = fs.readFileSync(filePath.replace(/\\/g, "/"), "utf-8");

        let lineas = data.split("\n");
        const lineaDeclaracionTest = lineas.find(
          (linea: string | string[]) => linea.includes("@Test") || linea.includes("@ParameterizedTest")
        );
      
        if (lineaDeclaracionTest === undefined) {
          return false;
        } else {
          return true;
        }
        
    }
}