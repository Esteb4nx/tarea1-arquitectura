import { JavaFile } from "../../../models/javaFile";

/**
 * Clase `Scanner` que representa un escáner de archivos Java en un proyecto.
 *
 * Esta clase mantiene una lista de todos los archivos Java en un proyecto y proporciona métodos para acceder y modificar esa lista.
 */
export class Scanner {
  /**
   * La ruta al directorio del proyecto.
   */
  private _projectPath: string;

  /**
   * Lista de todos los archivos Java en el proyecto.
   */
  private _filesOfProject: JavaFile[] = [];

  /**
   * Crea una nueva instancia de `Scanner`.
   *
   * @param projectPath - La ruta al directorio del proyecto.
   * @param filesOfProject - Una lista de todos los archivos Java en el proyecto.
   */
  constructor(projectPath: string, filesOfProject: JavaFile[]) {
      this._projectPath = projectPath;
      this._filesOfProject = filesOfProject;
  }

  /**
   * Obtiene la ruta al directorio del proyecto.
   * 
   * @returns La ruta al directorio del proyecto.
   */
  get projectPath() {
      return this._projectPath;
  }

  /**
   * Obtiene la lista de todos los archivos Java en el proyecto.
   * 
   * @returns La lista de todos los archivos Java en el proyecto.
   */
  get filesOfProject() {
      return this._filesOfProject;
  }

  /**
   * Establece la lista de todos los archivos Java en el proyecto.
   * 
   * @param files - La nueva lista de archivos Java.
   */
  set filesOfProject(files: JavaFile[]) {
      this._filesOfProject = files;
  }
}
