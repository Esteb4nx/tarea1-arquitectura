import { JavaFile } from "../../../models/javaFile";
import { Validator } from "../utils/validator";
import { Scanner } from "./Scanner";
const fs = require("fs");
const path = require("path");
const log = require("npmlog");


/**
 * Clase `ScannerBuilder` para construir una instancia de `Scanner`.
 * 
 * Esta clase se encarga de recorrer un directorio de proyecto, leer todos los archivos Java en él,
 * procesar la información relevante y generar una lista de `JavaFile` para posteriormente construir un objeto `Scanner`.
 */
export class ScannerBuilder {
  /**
   * La ruta al directorio del proyecto.
   */
  private _projectPath: string;

  /**
   * Crea una nueva instancia de `ScannerBuilder`.
   * 
   * @param projectPath
   */
  constructor(projectPath: string) {
    this._projectPath = projectPath;
  }

  /**
   * Construye un nuevo objeto `Scanner`.
   * 
   * Recorre el directorio del proyecto, procesando todos los archivos .java, recoge información sobre ellos 
   * y construye una lista de `JavaFile`. Finalmente, retorna una nueva instancia de `Scanner` que usa esta lista.
   *
   * @returns Una nueva instancia de `Scanner`.
   */
    build(): Scanner {
      const validator = new Validator();
      const filesOfProject: JavaFile[] = [];
  
      const stack = [this._projectPath];
  
      while (stack.length) {
        const currentDir = stack.pop();
  
        try {
          const files = fs.readdirSync(currentDir);
  
          files.forEach((file: JavaFile) => {
            const filePath = path.join(currentDir, file);
            const stat = fs.statSync(filePath);
  
            if (stat.isDirectory()) {
              stack.push(filePath);
            } else {
              if (filePath.includes(".java")) {
                const tmpArray = filePath.replace(/\\/g, "/").split("/");
                const fileName = tmpArray[tmpArray.length - 1];
                const type = validator.getType(filePath, fileName);
  
                if (fileName == "package-info.java") {
                  return;
                }
  
                if (validator.isTest(filePath)) {
                  return;
                }
  
                const data = fs.readFileSync(filePath.replace(/\\/g, "/"), "utf-8");
  
                const javaFile: JavaFile = {
                  fileName: fileName,
                  filePath: filePath.replace(/\\/g, "/"),
                  fileData: data,
                  type: type,
                };
  
                filesOfProject.push(javaFile);
              }
            }
          });
        } catch (err) {
          log.error("Error al leer directorio:", err);
        }
      }
  
      return new Scanner(this._projectPath, filesOfProject);
    }
  }