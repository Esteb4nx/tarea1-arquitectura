const fs = require("fs");
const path = require("path");
const log = require("npmlog");
import { IPlugin } from "../../../interfaces/IPlugin";
import { JavaFile } from "../../../models/javaFile";

export class Instability implements IPlugin {
  /**
   * Calcula la inestabilidad de un proyecto de software.
   * La inestabilidad es una medida de la proporción de dependencias eferentes y aferentes de un módulo.
   *
   * @param {JavaFile[]} files - Un arreglo de objetos que representan los archivos en el proyecto.
   * Cada objeto en el arreglo tiene una propiedad `fileName` que contiene el nombre del archivo, y una propiedad `filePath`
   * que contiene la ruta del archivo en el sistema de archivos.
   * @returns {number} El valor de la inestabilidad promedio del proyecto.
   */

  async execute(files: JavaFile[]): Promise<number> {
    let acomplamientoEferente;
    let acomplamientoAferente;
    let inestabilidadTotal = 0;
    log.info(`Analizando archivos...`);

    files.forEach((element) => {
      let data = fs.readFileSync(element.filePath.replace(/\\/g, "/"), "utf-8");
      acomplamientoEferente = this.getImports(data, files);
      acomplamientoAferente = this.getCalls(files, element);
      let inestabilidad = 0;
      if (acomplamientoAferente + acomplamientoEferente == 0) {
        inestabilidadTotal += inestabilidad;
      } else {
        inestabilidad =
          acomplamientoEferente /
          (acomplamientoAferente + acomplamientoEferente);
        inestabilidadTotal += inestabilidad;
      }
      // log.info(`Archivo: ${element.fileName} AE: ${acomplamientoEferente} AA: ${acomplamientoAferente} Inestabilidad: ${inestabilidad}`)
    });

    let inestabilidadPromedio = inestabilidadTotal / files.length;
    log.info(`La inestabilidad promedio es: ${inestabilidadPromedio}`);

    return inestabilidadPromedio;
  }

  /**
   * Calcula el acoplamiento eferente de un archivo en un proyecto de software.
   * El acoplamiento eferente mide cuántos módulos usan el módulo en cuestión.
   *
   * @param {string} data - El código fuente del archivo.
   * @param {JavaFile[]} files - Un arreglo de objetos que representan los archivos en el proyecto.
   * Cada objeto en el arreglo tiene una propiedad `fileName` que contiene el nombre del archivo.
   * @returns {number} El valor del acoplamiento eferente del archivo.
   */

  public getImports(data: string, files: JavaFile[]) {
    let acoplamientoEferente = 0;
    let lineas = data.split("\n");
    let imports = lineas.filter((linea) => linea.includes("import"));

    imports.forEach((linea) => {
      files.forEach((file) => {
        let fileNameWithSemicolon = file.fileName.split(".")[0] + ";";
        let fileNameToImportFunction = file.fileName.split(".")[0] + ".";
        if (
          linea.includes(fileNameWithSemicolon) ||
          linea.includes(fileNameToImportFunction)
        ) {
          acoplamientoEferente += 1;
        }
      });
    });

    return acoplamientoEferente;
  }

  /**
   * Calcula el acoplamiento aferente (incoming coupling) de un archivo en un proyecto de software.
   * El acoplamiento aferente mide cuántos módulos son usados por el módulo en cuestión.
   *
   * @param {JavaFile[]} files - Un arreglo de objetos que representan los archivos en el proyecto.
   * Cada objeto en el arreglo tiene una propiedad `fileName` que contiene el nombre del archivo, y una propiedad `filePath`
   * que contiene la ruta del archivo en el sistema de archivos.
   * @param {JavaFile} file - Un objeto que representa el archivo para el que se quiere calcular el acoplamiento aferente.
   * El objeto tiene una propiedad `fileName` que contiene el nombre del archivo, y una propiedad `filePath`
   * que contiene la ruta del archivo en el sistema de archivos.
   * @returns {number} El valor del acoplamiento aferente del archivo.
   */

  public getCalls(files: JavaFile[], file: JavaFile) {
    let acomplamientoAferente = 0;
    files = files.filter((element) => element != file);

    files.forEach((element) => {
      let data = fs.readFileSync(element.filePath.replace(/\\/g, "/"), "utf-8");
      if (data.includes(file.fileName.split(".")[0] + ";")) {
        acomplamientoAferente += 1;
      }
    });
    return acomplamientoAferente;
  }
}
