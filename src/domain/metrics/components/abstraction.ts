const fs = require("fs");
const path = require("path");
const log = require("npmlog");
import { JavaFile } from "../../../models/javaFile";
import { Type } from "../../../models/types";
import { IPlugin } from "../../../interfaces/IPlugin";

export class Abstraction implements IPlugin {
  async execute(files: JavaFile[]): Promise<number> {
    let degreeOfAbstraction = this.degreeOfAbstraction(files);
    log.info(`Grado de abstracción: ${degreeOfAbstraction}`);
    return degreeOfAbstraction;
  }

  /**
   * Calcula el grado de abstracción de un conjunto de archivos en un proyecto de software.
   * El grado de abstracción mide qué porcentaje de los archivos son abstractos.
   *
   * @param {Array<>} files - Un arreglo de objetos que representan los archivos en el proyecto.
   * Cada objeto en el arreglo tiene una propiedad `type` que indica si el archivo es abstracto o concreto.
   * El valor de `type` es "ABSTRACT" para los archivos abstractos y "CONCRETE" para los archivos concretos.
   * @returns {number} El valor del grado de abstracción del conjunto de archivos.
   */

  private degreeOfAbstraction(files: JavaFile[]) {
    let abstract = 0;
    let concrete = 0;

    files.forEach((element) => {
      if (element.type == Type.ABSTRACT) {
        abstract += 1;
      } else {
        concrete += 1;
      }
    });

    return abstract / (abstract + concrete);
  }
}
