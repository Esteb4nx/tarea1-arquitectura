import { PluginFactory } from "./PluginFactory";
import {IPlugin} from '../interfaces/IPlugin'
import { Abstraction } from "../domain/metrics/components/abstraction";

export class AbstractionPluginFactory implements PluginFactory {
    createPlugin(): IPlugin {
      return new Abstraction();
    }
  }