import { Instability } from "../domain/metrics/components/instability";
import { PluginFactory } from "./PluginFactory";
import {IPlugin} from '../interfaces/IPlugin'

export class InstabilityPluginFactory implements PluginFactory {
    createPlugin(): IPlugin {
      return new Instability();
    }
  }