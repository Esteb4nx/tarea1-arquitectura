import { Instability } from "../domain/metrics/components/instability";
import { PluginFactory } from "./PluginFactory";
import {IPlugin} from '../interfaces/IPlugin'
import { Chart } from "../domain/charts/components/charts";

export class ChartPluginFactory implements PluginFactory {
    createPlugin(): IPlugin {
      return new Chart();
    }
  }