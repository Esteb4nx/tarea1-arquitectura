import {IPlugin} from '../interfaces/IPlugin'

export interface PluginFactory {
    createPlugin(): IPlugin;
}