import { Type } from "./types";

/**
 * Interfaz `JavaFile` para representar un archivo Java.
 *
 * La interfaz define las propiedades esenciales que debe tener un archivo Java, incluyendo su nombre, ruta, datos y tipo.
 */
export interface JavaFile {
    /**
     * Nombre del archivo Java.
     */
    fileName: string;
    
    /**
     * Ruta completa del archivo Java en el sistema de archivos.
     */
    filePath: string;
    
    /**
     * Contenido del archivo Java representado como una cadena de texto.
     */
    fileData: string;
    
    /**
     * Tipo del archivo Java, como por ejemplo una clase, una interfaz, una enumeración, etc.
     */
    type: Type;
}
