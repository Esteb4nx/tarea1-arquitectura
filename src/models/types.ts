/**
 * Enumeración `Type` para representar los tipos posibles de un archivo Java.
 *
 * Esta enumeración incluye tres opciones: ABSTRACT para archivos Java que representan tipos abstractos
 * (como clases abstractas e interfaces), CONCRETE para archivos Java que representan tipos concretos
 * (como clases normales) y OTHER para cualquier otro tipo de archivo Java que no encaje en las categorías anteriores.
 */
export enum Type{

    ABSTRACT,

    CONCRETE,

    OTHER
}
