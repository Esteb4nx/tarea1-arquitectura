import { Chart } from "./domain/charts/components/charts";
import { Abstraction } from "./domain/metrics/components/abstraction";
import { Instability } from "./domain/metrics/components/instability";
import { ScannerBuilder } from "./domain/scanner/components/ScannerBuilder";
import { AbstractionPluginFactory } from "./factory/AbstractionPluginFactory";
import { ChartPluginFactory } from "./factory/ChartPluginFactory";
import { InstabilityPluginFactory } from "./factory/InstabilityPluginFactory";


/**
 * Función principal para ejecutar el análisis de un proyecto.
 * 
 * La función comienza extrayendo argumentos de la línea de comando, incluyendo la ruta del proyecto y los plugins a usar.
 * Luego, se inicializan las fábricas de plugins y se crean las instancias de los plugins a utilizar.
 * 
 * El proyecto es escaneado, recogiendo todos los archivos relevantes, y cada plugin activo se ejecuta sobre estos archivos.
 * 
 * Por último, se recoge y muestra la información generada por los plugins y se escribe la información de registro en un archivo.
 * 
 * @async
 */
async function main() {
  const log = require("npmlog");
  const logfile = require("npmlog-file");
  const [projectPath, ...plugins] = process.argv.slice(2);

  if (!projectPath) {
    log.error(
      "Argumento faltante, ingrese el comando -> npm run dev PROJECT_PATH"
    );
    return;
  }

  const IMAGE_NAME = "DistanciaDeLaSecuenicaPrincipal.png";
  const scanner = new ScannerBuilder(projectPath).build();

  type PluginFactories = {
    abstraction: AbstractionPluginFactory;
    instability: InstabilityPluginFactory;
    chart: ChartPluginFactory;
    
  };

  const pluginFactories: PluginFactories = {
    abstraction: new AbstractionPluginFactory(),
    instability: new InstabilityPluginFactory(),
    chart: new ChartPluginFactory(),
  };

  const activePlugins = plugins
    .map((pluginName) => {
      if (pluginName in pluginFactories) {
        const factory = pluginFactories[pluginName as keyof PluginFactories];
        return factory && factory.createPlugin();
      }
    })
    .filter(Boolean);

  const activePluginNames = activePlugins
    .filter(Boolean)
    .map((plugin) => plugin!.constructor.name);
  if (
    activePluginNames.includes("Chart") &&
    (!activePluginNames.includes("Abstraction") ||
      !activePluginNames.includes("Instability"))
  ) {
    log.error(
      "Para usar el plugin 'Chart', los plugins 'Abstraction' e 'Instability' también deben estar activados."
    );
    return;
  }

  try {
    log.info("Escaneando proyecto...");
    const files = scanner.filesOfProject;

    if (!files) {
      log.error("Directorio no válido");
      return;
    }

    log.info("Proyecto escaneado con éxito");

    let abs: number = NaN;
    let ins: number = NaN;
    let distance: number = NaN;

    for (const plugin of activePlugins) {
      if (plugin instanceof Abstraction) {
        abs = await plugin.execute(files);
      }
      if (plugin instanceof Instability) {
        ins = await plugin.execute(files);
      }
      if (plugin instanceof Chart) {
        distance = await plugin.execute({ abs, ins, imageName: IMAGE_NAME });
      }
    }

    const pluginData = activePlugins.map((plugin) => {
      if (plugin instanceof Abstraction) {
        return { plugin: "Abstraction", value: abs };
      }
      if (plugin instanceof Instability) {
        return { plugin: "Instability", value: ins };
      }
      if (plugin instanceof Chart) {
        return { plugin: "Distance of main", value: distance };
      }
      return {};
    });

    console.table(pluginData);
  } catch (error) {
    log.error(
      "Directorio no valido, revise la ruta ingresada o cambie de proyecto"
    );
  }

  logfile.write(log, `logs/log-${Date.now()}.log`);
}

main();
